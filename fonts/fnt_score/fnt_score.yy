{
    "id": "54f003f5-4387-405d-afae-d1e3ba1d9b76",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_score",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Agency FB",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "051d680e-dbd0-4caa-9d2a-2d7f0781183e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 34,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 103,
                "y": 146
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "126129a6-ae8d-4582-8ad2-b627c250d9ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 34,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 137,
                "y": 146
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "b49d65c7-f0e4-4ff6-98f7-bbf90cb0a848",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 34,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 32,
                "y": 146
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "760ce52a-b883-4377-beda-f201b7b2c73b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 34,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 77,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "1111e237-cf1d-49b0-b5dd-d8341fe81860",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 34,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 208,
                "y": 38
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "4a7c0b95-2b76-493e-91c3-001bf448b233",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 34,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "d87fa04b-4455-4a5b-b460-82d86c347d30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 34,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 127,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "0afd2574-426e-41f5-80c6-db93996a49f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 34,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 143,
                "y": 146
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "6be305ae-cccb-444b-8294-369275f176d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 34,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 60,
                "y": 146
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "3ac88145-f8d8-43a2-86e5-909da256cdd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 34,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 22,
                "y": 146
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "eb8f6ce4-dc95-4383-83c5-668a1878458f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 34,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 234,
                "y": 38
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "85eae67f-5177-4d53-93d8-ad1913e081b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 34,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 114,
                "y": 38
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "bb0a9063-85ad-4c34-85c8-bd5a7d57d032",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 34,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 118,
                "y": 146
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "9cddf953-8e98-4532-bb86-cd1d67c9d7c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 34,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 42,
                "y": 146
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "fa8a036c-adaa-462a-ac4b-5c6b8dfd455f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 34,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 149,
                "y": 146
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "a83ae745-2bb1-4ff1-8e97-2fbd1ee6a73f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 34,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 142,
                "y": 38
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "6c748c33-9d2a-44cd-bd78-bd29ff1c6075",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 34,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 54,
                "y": 74
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "def28116-bce1-4a63-a42b-d3e95c3cb0c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 34,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 87,
                "y": 146
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "e70194c7-691f-4d19-8c1f-4f35815e0aed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 34,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 128,
                "y": 38
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "61e96a21-fc7e-4232-864a-d400748ae9c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 34,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 145,
                "y": 74
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "ccad9022-3d96-46e7-9f5c-30872171be23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 34,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 172,
                "y": 2
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "2b31c29c-4ae7-4016-b849-c50f81da6c3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 34,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 156,
                "y": 38
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "43ba4a44-d4d6-4059-b1ea-91b1a3ce8ff8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 34,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 221,
                "y": 38
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "bab3f3c9-a83e-4027-af7d-1822a0aa8b1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 34,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 86,
                "y": 38
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "604cdc1f-6016-4659-8621-892b5896d2b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 34,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 184,
                "y": 74
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "6d5e71e9-7fdb-4d69-9a4f-76e426d4d23f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 34,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 93,
                "y": 74
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "865d4765-9b7b-4aa7-a93c-64556d360e44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 34,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 131,
                "y": 146
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "5c618f24-9fad-4c6c-a96d-c3c0006bab35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 34,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 111,
                "y": 146
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "aef751b0-fe55-4b79-b481-085f44e66d0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 34,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 122,
                "y": 110
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "dc6ecf40-70d9-4b2b-8b38-dc1727366c1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 34,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 106,
                "y": 74
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "4a30f8fd-7dfa-4a37-bf4f-8b65511e5ad2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 34,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 238,
                "y": 110
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "14bab3b4-d14a-470d-b12d-ad75a6a885e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 34,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 170,
                "y": 110
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "2be73c4c-03ef-4cc3-902f-4774c9197add",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "69511742-f254-484a-97b7-ac00b17bbecb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 34,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 142,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "2eb2d536-2d23-4364-b74b-6bdcdbdcd460",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 34,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 171,
                "y": 74
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "b943fcfb-c952-49b8-aeac-c3dee844a87e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 34,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 158,
                "y": 74
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "0263e442-2a69-4c5d-9ef8-b73c7286ca2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 34,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 119,
                "y": 74
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "36a4ae01-a98e-48c7-9f8f-274656ec4c7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 34,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 158,
                "y": 110
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "5fa072d4-055c-4241-90be-f7f713f302b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 34,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 146,
                "y": 110
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "055287fe-f6e6-40d8-a8b8-ec02940b4075",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 34,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 132,
                "y": 74
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "4945b45c-11a2-4626-a59d-6d84f0d62661",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 34,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 202,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "56edcf79-3a08-49b4-9d7a-5a36bc49ff6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 34,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 155,
                "y": 146
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "1847148c-b584-4163-a3c1-cf4bdeca9dc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 34,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 67,
                "y": 74
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "0fd5133a-49f2-40db-bc09-7a5eb148c1dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 34,
                "offset": 1,
                "shift": 12,
                "w": 12,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "a1f74495-1a93-4290-9f88-dfb9a246edef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 34,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 216,
                "y": 110
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "802527ca-a71d-4e59-8c1a-dcc21b599756",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 34,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "255bbd6d-be76-4b8f-be70-25efd6b1b9b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 34,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 72,
                "y": 38
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "79dd3b8e-774f-47e3-bd8a-344e6756265f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 34,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 230,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "2cb92f80-08de-4778-922e-6ed43aac6b38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 34,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 41,
                "y": 74
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "503df7dc-e10b-4679-b637-921c0ae27f4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 34,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 187,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "6866c59c-f226-4c64-bca7-9f5d24f90436",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 34,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 38
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "5a351d90-3c3c-49f3-bf7b-b4de740e68ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 34,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 28,
                "y": 74
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "9c02ffc7-413e-4e5f-ad30-e769fc10db36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 34,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 15,
                "y": 74
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "7a8795f1-572d-4152-b778-134c8a7f39fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 34,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "e48d7dba-545a-4758-a33f-b3d57f02e0a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 34,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 16,
                "y": 38
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "11acab48-3a96-4a6c-9337-3977f9a27563",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 34,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "7faca339-bdde-405a-8aab-b7c5bd17ef23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 34,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 30,
                "y": 38
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "46576f8d-0ed0-4df4-b544-906fa63ee3d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 34,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 157,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "fa99c724-096d-4699-b47b-7d5aed592310",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 34,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 80,
                "y": 74
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "e6041f7a-5d59-41f4-bfa7-8cd36a5b8811",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 34,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 69,
                "y": 146
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "e46dee2e-da07-4a11-8383-55ab8491c295",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 34,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 44,
                "y": 38
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "904840d7-95b3-4681-bf48-fe70c49c6d10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 34,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 78,
                "y": 146
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "f258252b-6be4-44d5-a33a-c1a0378330aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 34,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 74,
                "y": 110
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "12b9fa7b-a192-4a4e-96fc-9213559867e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 34,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 58,
                "y": 38
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "71085f44-c0f1-437d-ade3-9cd97ed750e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 34,
                "offset": 3,
                "shift": 11,
                "w": 7,
                "x": 51,
                "y": 146
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "cc09abe6-da4b-490c-b2ed-130fc53a4812",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 34,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 209,
                "y": 74
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "a84382fb-f9c7-4192-9bcf-357738524b13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 34,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 221,
                "y": 74
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "30250e7f-c588-4d0e-9b4d-b33fc7ea8ced",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 34,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 98,
                "y": 110
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "93146f1b-d839-44cb-bb93-74efb087ae43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 34,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 233,
                "y": 74
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "173d9c38-d37c-4ec4-85cf-2c35a58fa91e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 34,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 110
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "2233325b-4271-42dc-94fe-12a57fe48b58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 34,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 2,
                "y": 146
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "64398fcf-dfb4-4790-885a-206bee2660d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 34,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 14,
                "y": 110
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "babe69cd-15d0-409d-b1b5-478407618d9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 34,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 26,
                "y": 110
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "9f2e7cb0-79db-4e01-87cb-2fd4c0eb72fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 34,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 125,
                "y": 146
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "b10f930d-d6b9-4cc2-963e-40a7815eabec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 34,
                "offset": -1,
                "shift": 5,
                "w": 6,
                "x": 95,
                "y": 146
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "3fa302d7-bae1-4659-9d35-e464879e0f69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 34,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 38,
                "y": 110
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "306790d8-7bf9-49f5-b04d-5fcad0335b85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 34,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 167,
                "y": 146
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "9c0c4715-54b2-4bc1-b4d2-e1d9b01cab2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 34,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 42,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "d51efdbd-ca28-46ac-b4cf-83e3173cacf2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 34,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 50,
                "y": 110
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "57dae5d8-e2e2-4355-9e90-cea2166c1425",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 34,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 62,
                "y": 110
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "24826d16-658a-4865-9934-52d12e190713",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 34,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 86,
                "y": 110
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "b243a2c0-9b75-4bbf-8228-15793ec79eae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 34,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 110,
                "y": 110
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "e44f75d1-bd1b-4071-9dfc-4c40ab215c60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 34,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 194,
                "y": 110
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "09bb8f7a-d857-4416-9ed7-a29901bdf6e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 34,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 205,
                "y": 110
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "9593b47f-8e5a-42ad-8a02-b866a2e071b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 34,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 12,
                "y": 146
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "c3dd3a6c-df85-4ab7-8077-1ef35f9e2303",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 34,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 197,
                "y": 74
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "f93a2689-7d3a-43f7-a0cc-9d4c1632bc4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 34,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 195,
                "y": 38
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "dab3adc8-8e6d-4914-89a7-957d1c2d6ae1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 34,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "900a0573-acb5-4646-846f-4925a725dc0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 34,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 182,
                "y": 38
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "a11d9de4-11b3-4a8a-a3a2-4baacaa01012",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 34,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 169,
                "y": 38
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "fa6c3920-0c23-46ba-8d68-1e87d7b9fdde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 34,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 227,
                "y": 110
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "8b752f5c-3909-4835-a0f4-521974758939",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 34,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 134,
                "y": 110
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "6b5cc363-18c1-4eb7-885e-9e29e6305afb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 34,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 161,
                "y": 146
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "e500f117-2055-42bb-b0a2-c71df258ad11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 34,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 182,
                "y": 110
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "40830995-5a0f-4710-92e2-7a381c172110",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 34,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 100,
                "y": 38
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "5343",
    "size": 21,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}