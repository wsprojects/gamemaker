{
    "id": "1c8ea627-f1ba-4db4-95b8-e9276536fc1c",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_button",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Agency FB",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "3a83eb8f-85a8-4440-b34c-3fcbfa848767",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 61,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 246,
                "y": 191
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "9f88f692-0e7b-48fc-b5f6-9e9c19b962e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 61,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 310,
                "y": 191
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "5d4b136c-ade4-445a-97dd-d5d6287bc5dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 61,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 93,
                "y": 191
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "030e04ba-72b4-406c-b009-69953ae879ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 61,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "a99f3d5a-b3a4-4a12-9016-3d317b213e62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 61,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 150,
                "y": 128
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "2788b6d2-fc53-48e2-869e-3a2a65eaf8cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 61,
                "offset": 1,
                "shift": 33,
                "w": 32,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "88b723fa-ba73-43a9-97d0-4807df921647",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 61,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 406,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "893d99ae-15eb-411e-a74c-4b58ed34cfcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 61,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 346,
                "y": 191
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "5538d3f4-d2b3-42e2-887e-b57c1045762d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 61,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 203,
                "y": 191
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "86423172-8b76-4207-b67f-0aae424bd9b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 61,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 188,
                "y": 191
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "0bee61a8-f0a1-4549-9c2d-fe94535092de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 61,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "1d2b7c73-0ab3-4605-89d1-3e72a7dcd7eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 61,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 310,
                "y": 2
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "82cffc2b-3206-4637-8a93-e5924d0fa7bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 61,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 282,
                "y": 191
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "ae91cf88-6fd3-4f5e-aae6-657d7b8a1b72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 61,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 173,
                "y": 191
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "127fc7bd-e0a4-4ddd-bb3d-182f034bfd85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 61,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 292,
                "y": 191
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "dd8e2f89-3276-4d4a-8ac8-05abbb8bf045",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 61,
                "offset": 0,
                "shift": 23,
                "w": 22,
                "x": 382,
                "y": 2
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "d98f484a-e25c-4e16-9294-02f1006026ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 61,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 444,
                "y": 65
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "f6dc0ef7-32c3-41b0-93bd-3180755d92a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 61,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 271,
                "y": 191
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "1bca0d3d-94f7-4242-adce-ae7a4f630fda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 61,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 2,
                "y": 128
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "6b300b03-1560-4cb8-b6ca-589e6fb5e2e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 61,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 356,
                "y": 65
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "1bd84d61-c76a-4767-aefd-dba28d2c9ed3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 61,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 261,
                "y": 2
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "5b97a06f-ee4d-4d0f-a20c-02bf4e36b3b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 61,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 129,
                "y": 128
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "9f7e2560-327e-4b40-8810-e68195e0a81e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 61,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 400,
                "y": 65
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "0aecb559-f8e2-45e7-9bed-d66204352041",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 61,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 422,
                "y": 65
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "7eb95ad4-4795-40cc-9c05-b804e417dcc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 61,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 466,
                "y": 65
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "d6ea8086-10f2-48b4-8776-38478da31136",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 61,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 488,
                "y": 65
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "7da3e6ab-307f-4650-abfe-bff3a234075a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 61,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 301,
                "y": 191
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "cf9bf352-1396-4bba-b2db-b10638ca0d95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 61,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 328,
                "y": 191
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "702f7d47-2800-4823-b5ee-3a09e8aecd52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 61,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 57,
                "y": 191
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "b8ea37c0-5ada-42c6-9263-499c4ff624da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 61,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 378,
                "y": 65
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "a8406ee5-8798-424e-80b3-4acc2f524eff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 61,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 110,
                "y": 191
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "d272d6b9-24a8-4b1c-a9d9-6b81b76b30ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 61,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 87,
                "y": 128
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "b36a59e2-75a2-46b7-a764-0b29b6fff44c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 61,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 157,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "18aa03d8-3421-4822-9835-47379cc60681",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 61,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "a2a5072a-7eed-420f-88fb-76189048e00b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 61,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 268,
                "y": 65
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "95f801dc-073e-44f4-bebb-c463eac4bb90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 61,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 246,
                "y": 65
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "9108d8f2-b27d-43c1-8492-c921fee05ad3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 61,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 224,
                "y": 65
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "75e9a51f-5745-43c8-9a73-0788701d6ef8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 61,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 2,
                "y": 191
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "ca7a9623-ba29-4c5d-81e0-35f495959b26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 61,
                "offset": 2,
                "shift": 19,
                "w": 17,
                "x": 476,
                "y": 128
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "97141466-45fe-4bd0-ba53-be73fbf4e53d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 61,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 202,
                "y": 65
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "7c6bde9e-6b13-437a-86d7-6b9a2cc6535f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 61,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 180,
                "y": 65
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "07e04e18-44a7-42cd-a23d-e6718cf7d265",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 61,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 319,
                "y": 191
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "d97c4d0c-c51b-4c9b-990d-6f1943ef05d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 61,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 66,
                "y": 128
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "7eb29c92-c8cc-4092-bee4-31d14912b025",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 61,
                "offset": 2,
                "shift": 22,
                "w": 20,
                "x": 158,
                "y": 65
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "0935e51f-7434-4cc4-8932-1c51b2cda397",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 61,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 21,
                "y": 191
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "c8522fb6-17fb-4e69-b5ef-4e03771b0612",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 61,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 185,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "7a5e08c9-196c-4302-971b-b5187dfeda4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 61,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 453,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "d58c8ac1-5a24-4b4f-8f1c-fcdff12b0444",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 61,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 312,
                "y": 65
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "c20b4f57-5cda-4bbc-b007-385bc4420470",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 61,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 92,
                "y": 65
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "fa169466-ef14-4ae4-9c58-9844216c85bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 61,
                "offset": 2,
                "shift": 25,
                "w": 23,
                "x": 236,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "b3f678e3-26d1-4120-84bf-c6d4f857dffc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 61,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 430,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "2fd2aa68-25e1-4b2d-8752-844ab6170325",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 61,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 48,
                "y": 65
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "1fa91dd5-6776-4f3f-a9ae-5f784fafb219",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 61,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 290,
                "y": 65
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "1f7182e5-8e47-4690-be9b-249fda59435f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 61,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 70,
                "y": 65
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "764a88ed-9929-46db-a560-c84754244ca0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 61,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 476,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "a3e51d1b-df2b-4aac-a5f5-5db63fb55e6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 61,
                "offset": 0,
                "shift": 32,
                "w": 32,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "aac1e824-272f-4361-8ef6-ef3bb840cfce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 61,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 358,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "c86c7b4a-704d-4713-8128-131626e00d3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 61,
                "offset": 0,
                "shift": 23,
                "w": 22,
                "x": 334,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "8767f47c-7759-46bc-ab0f-0be4a523d9bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 61,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 24,
                "y": 128
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "ba8c1514-f7fd-4829-ab71-38ccb8bc7072",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 61,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 218,
                "y": 191
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "5eda550f-f4dd-4d74-ba68-daf5f75342e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 61,
                "offset": 0,
                "shift": 23,
                "w": 22,
                "x": 286,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "db95e298-33bf-4727-9b29-bfd0b3fcdafa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 61,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 232,
                "y": 191
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "d6b00eaa-0812-4f21-8786-8c3f90f648d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 61,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 108,
                "y": 128
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "7e974204-fff8-4376-96f3-5ccd9f558a44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 61,
                "offset": 0,
                "shift": 20,
                "w": 21,
                "x": 25,
                "y": 65
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "caf610c7-d576-4bf3-9016-40082a13aa15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 61,
                "offset": 5,
                "shift": 21,
                "w": 13,
                "x": 158,
                "y": 191
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "1747fd93-5a0c-4031-92a3-ffee4bae3d6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 61,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 171,
                "y": 128
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "677f7526-6613-49a8-b464-83b17708583c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 61,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 210,
                "y": 128
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "54abd49a-98b2-4c08-839b-25757612c444",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 61,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 324,
                "y": 128
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "5ec9d191-d750-43c6-b43b-a9199d487273",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 61,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 229,
                "y": 128
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "d975a76b-982f-4b6c-b104-f6b09f247059",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 61,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 248,
                "y": 128
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "7d805f3b-dc24-47a5-9d62-2651621ac9fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 61,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 127,
                "y": 191
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "f54586a8-1582-4b23-8594-e70d012ca44a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 61,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 267,
                "y": 128
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "51ad02fd-0d15-4378-b38a-a97805fe38ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 61,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 286,
                "y": 128
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "4e483b7f-99cd-48a9-b882-79cf09cd31ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 61,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 363,
                "y": 191
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "21b81568-cd8b-4c69-bbb5-b19eaf095ac0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 61,
                "offset": -2,
                "shift": 10,
                "w": 10,
                "x": 259,
                "y": 191
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "e62b3abf-c075-43b8-8733-8c87f199a869",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 61,
                "offset": 2,
                "shift": 19,
                "w": 17,
                "x": 362,
                "y": 128
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "b67df107-8cfd-4664-9044-23b126a63e56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 61,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 355,
                "y": 191
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "4e616c33-9545-44f3-867c-972acef236d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 61,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 70,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "3d4a13c2-1f80-4089-9f62-d5da5c42e486",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 61,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 419,
                "y": 128
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "3d5a626b-335e-4a3e-aa63-3524a9c7a136",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 61,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 305,
                "y": 128
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "59793c88-4788-432f-980d-212889955024",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 61,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 191,
                "y": 128
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "c363629d-5b4e-4040-a14b-4b1f5ac6416e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 61,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 343,
                "y": 128
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "3523452e-16be-4d7e-b714-3414dfab8af3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 61,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 75,
                "y": 191
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "e9b586a2-bac4-4986-8f3c-075cfc8874b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 61,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 381,
                "y": 128
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "e552ef35-6949-4828-8ed5-2d206beed189",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 61,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 143,
                "y": 191
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "a4b07453-c270-4113-aebf-298bd9226fb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 61,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 400,
                "y": 128
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "5e9287f9-dd54-4e06-afea-b4b2005672fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 61,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 114,
                "y": 65
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "209c4747-0cce-4b63-a115-657286a8adeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 61,
                "offset": 0,
                "shift": 28,
                "w": 27,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "1b6dbced-fc9e-4e72-8e30-ff069c321e12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 61,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 45,
                "y": 128
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "638e3913-0c7b-466c-a884-ee5d4bc3f34e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 61,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 136,
                "y": 65
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "829bf56e-4362-451b-98b2-0647e17a3c08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 61,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 39,
                "y": 191
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "a72d745b-1764-4fd5-a504-67b73065e4a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 61,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 457,
                "y": 128
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "d77eca16-7a4d-47f2-a487-3eb241d56da5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 61,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 337,
                "y": 191
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "be7e89dc-34f6-4df9-aa63-3869031ef5f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 61,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 438,
                "y": 128
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "e0e01e1c-6196-4dd3-b59f-22fc39a3eea1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 61,
                "offset": 3,
                "shift": 26,
                "w": 20,
                "x": 334,
                "y": 65
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "5343",
    "size": 38,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}