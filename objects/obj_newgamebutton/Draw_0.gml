/// @author Wouter van der Heijden 30-3-2018
/// @description [Draw event obj_newgamebutton]

draw_self();

draw_set_font(fnt_button);
draw_set_colour(c_white);
draw_text(x- 90,y-30,"New Game");
