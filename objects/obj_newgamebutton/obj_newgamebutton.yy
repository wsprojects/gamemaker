{
    "id": "f1313c62-a0a1-4cca-98dc-9c11ae4f3ad4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_newgamebutton",
    "eventList": [
        {
            "id": "c90884ab-0192-4d15-8ee1-26a3d1f4b461",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "f1313c62-a0a1-4cca-98dc-9c11ae4f3ad4"
        },
        {
            "id": "29dd202b-c0f8-4700-9047-b2427c633a8a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f1313c62-a0a1-4cca-98dc-9c11ae4f3ad4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "bb8784aa-8859-424f-b902-13a1c48086d1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0003f5f2-4bf6-4a9c-a54a-4b022dd9642a",
    "visible": true
}