/// @author Wouter van der Heijden 31-3-2018
/// @description [Create event obj_enemydeath]

image_alpha = 1;
new = true;

spr1 = spr_yellowdeath;
spr2 = spr_yellowdeath;
spr3 = spr_yellowdeath;

image_xscale = 0.55;
image_yscale = image_xscale;;