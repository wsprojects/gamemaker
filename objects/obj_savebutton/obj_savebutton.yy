{
    "id": "f1045613-7355-422f-8c46-d21513fe2141",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_savebutton",
    "eventList": [
        {
            "id": "55261ef0-62c0-48b2-baec-53294d3028c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f1045613-7355-422f-8c46-d21513fe2141"
        },
        {
            "id": "8d14f7e1-2d80-4aa3-9852-53e3336b9026",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "f1045613-7355-422f-8c46-d21513fe2141"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "bb8784aa-8859-424f-b902-13a1c48086d1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0003f5f2-4bf6-4a9c-a54a-4b022dd9642a",
    "visible": true
}