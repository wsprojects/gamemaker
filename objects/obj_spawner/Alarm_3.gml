/// @author Wouter van der Heijden 31-3-2018
/// @description [Alarm 3 event obj_powerup]

instance_create_layer(random_range(100, room_width-100), random_range(100, room_height-100), "EnemyLayer", obj_powerup);
alarm[3] = irandom_range(sr_pup[0],sr_pup[1]);