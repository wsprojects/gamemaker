/// @author Wouter van der Heijden 31-3-2018
/// @description [Alarm 1 event obj_spawner]

instance_create_layer(random(room_width), random(room_height), "EnemyLayer", obj_bluespawn);
alarm[1] = irandom_range(sr_b[0],sr_b[1]);
