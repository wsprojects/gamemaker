/// @author Wouter van der Heijden 31-3-2018
/// @description [Alarm 0 event obj_spawner]

instance_create_layer(random(room_width), random(room_height), "EnemyLayer", obj_enemyspawn);
alarm[0] = irandom_range(sr_g[0],sr_g[1]);
