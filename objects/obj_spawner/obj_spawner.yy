{
    "id": "66a5045d-f8e9-4e8e-8226-efe231377084",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_spawner",
    "eventList": [
        {
            "id": "e7d7e09c-3af9-4824-8d2f-7f67e83a522c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "66a5045d-f8e9-4e8e-8226-efe231377084"
        },
        {
            "id": "55b949a3-6dc7-4481-b76a-e19d76a08e82",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "66a5045d-f8e9-4e8e-8226-efe231377084"
        },
        {
            "id": "8bbe60bd-d368-4081-ba00-1b59ce541d18",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "66a5045d-f8e9-4e8e-8226-efe231377084"
        },
        {
            "id": "c49e13d3-1add-450e-ae3a-18cb652a3e00",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "66a5045d-f8e9-4e8e-8226-efe231377084"
        },
        {
            "id": "4840ca7a-6e4b-4753-b6eb-47b8cbbc086b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "66a5045d-f8e9-4e8e-8226-efe231377084"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}