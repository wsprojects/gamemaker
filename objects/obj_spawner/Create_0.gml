/// @author Wouter van der Heijden 31-3-2018
/// @description [Create event obj_spawner]

sr_g = [60,90];
sr_b = [240,480];
sr_p = [500,1000];

sr_pup = [500,1000];

alarm[0] = irandom_range(sr_g[0],sr_g[1]);
alarm[1] = irandom_range(sr_b[0],sr_b[1]);
alarm[2] = irandom_range(sr_p[0],sr_p[1]);
alarm[3] = irandom_range(sr_pup[0],sr_pup[1]);