{
    "id": "f0cc56ac-8f4f-424f-833d-2e2b56fc1bdc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemyspawn",
    "eventList": [
        {
            "id": "c6e3b010-29a6-4a7a-9645-6009c18fb1eb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f0cc56ac-8f4f-424f-833d-2e2b56fc1bdc"
        },
        {
            "id": "716118d0-09f0-4cc5-b231-59d256446f04",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f0cc56ac-8f4f-424f-833d-2e2b56fc1bdc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "aad589e9-fefc-45cd-8c07-305cf0b87498",
    "visible": true
}