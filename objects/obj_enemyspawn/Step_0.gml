/// @author Wouter van der Heijden 31-3-2018
/// @description [Step event obj_enemyspawn]

image_xscale = min(image_xscale+spawnrate*size,size);
image_yscale = image_xscale;

if (image_xscale == size) instance_change(enemy, true);

image_angle = point_direction(x, y, obj_player.x, obj_player.y);