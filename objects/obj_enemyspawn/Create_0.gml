/// @author Wouter van der Heijden 31-3-2018
/// @description [Create event obj_enemyspawn]

image_xscale = 0.1;
image_yscale = 0.1;

size = 1;
spawnrate = 0.02;
enemy = obj_enemy;