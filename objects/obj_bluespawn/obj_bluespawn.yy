{
    "id": "5a87c486-03fc-4f25-904d-b5c1c5b4679c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bluespawn",
    "eventList": [
        {
            "id": "9c0efc98-10c2-48a7-acb1-5cf78594dea0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5a87c486-03fc-4f25-904d-b5c1c5b4679c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f0cc56ac-8f4f-424f-833d-2e2b56fc1bdc",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e1a98d3e-29b6-436a-a9e2-ae5da8f451a7",
    "visible": true
}