{
    "id": "ed0d9cc3-6fc0-44eb-97ac-e6562f65a565",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_purpledeath",
    "eventList": [
        {
            "id": "6c76c41b-f8e9-451d-bd62-36cfacb4e90f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ed0d9cc3-6fc0-44eb-97ac-e6562f65a565"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "39c8117a-ea66-409c-b5cd-a7fbbce3b885",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}