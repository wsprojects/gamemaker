/// @author Wouter van der Heijden 31-3-2018
/// @description [Create event obj_enemydeath]

image_alpha = 1;
new = true;

spr1 = spr_purpledeath;
spr2 = spr_purpledeath;
spr3 = spr_purpledeath;

image_xscale = 1.9;
image_yscale = image_xscale;
