/// @author Wouter van der Heijden 30-3-2018
/// @description [Collision event obj_bullet, obj_enemy]

with(other){
	hp -= 1;
}

instance_destroy();