/// @author Wouter van der Heijden 30-3-2018
/// @description [Create event obj_bullet]

//Direction + Randomness
direction = point_direction(x, y, mouse_x, mouse_y);
direction = direction + random_range(-4,4);

speed = 20
image_angle = direction;

step_counter = 0;
main_angle = direction;