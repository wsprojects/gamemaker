{
    "id": "91fe6fab-10e0-4bf9-b2d7-3ad719f7d6ae",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet",
    "eventList": [
        {
            "id": "fef8fc54-b895-4b85-bdee-cfc9fd2c1f8d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "91fe6fab-10e0-4bf9-b2d7-3ad719f7d6ae"
        },
        {
            "id": "d2a1d721-ef25-4909-8966-31949d974983",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "dcf498de-075d-4ee7-bc6a-66c0dba2c1d1",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "91fe6fab-10e0-4bf9-b2d7-3ad719f7d6ae"
        },
        {
            "id": "c5d9d483-ccf6-4ab9-b7b3-331e39207616",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "91fe6fab-10e0-4bf9-b2d7-3ad719f7d6ae"
        },
        {
            "id": "e85bed17-e2fa-4cb8-8654-79fd5b795f64",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "bbaa4e7d-c82d-4fc9-acf4-4b621b5acea4",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "91fe6fab-10e0-4bf9-b2d7-3ad719f7d6ae"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a8c0d942-b6e0-431f-97ee-090d42838226",
    "visible": true
}