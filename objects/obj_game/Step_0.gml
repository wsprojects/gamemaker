/// @author Wouter van der Heijden 30-3-2018
/// @description [Step event obj_game]

if (room == rm_title) && !instance_exists(obj_newgamebutton) {
	instance_create_layer(cw/2,ch/2 + 25, "ButtonLayer", obj_newgamebutton);
	instance_create_layer(cw/2,ch/2 + 150, "ButtonLayer", obj_savebutton);
	instance_create_layer(cw/2,ch/2 + 275, "ButtonLayer", obj_exitbutton);
}
