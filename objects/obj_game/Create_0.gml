/// @author Wouter van der Heijden 30-3-2018
/// @description [Create event obj_game]

cw = camera_get_view_width(view_camera[0]);
ch = camera_get_view_height(view_camera[0]); 
if file_exists("Save.txt"){
	var LoadFile = file_text_open_read("Save.txt");
	global.highscore = file_text_read_real(LoadFile);
	file_text_close(LoadFile);
} else {
	global.highscore = 0;
}
audio_play_sound(snd_music, 100, true);