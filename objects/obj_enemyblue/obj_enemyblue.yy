{
    "id": "11a82a8a-8a11-428f-bd1b-b24e42936e75",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemyblue",
    "eventList": [
        {
            "id": "0ba0d4e7-0453-4732-9890-626d612b742e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "11a82a8a-8a11-428f-bd1b-b24e42936e75"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "dcf498de-075d-4ee7-bc6a-66c0dba2c1d1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e1a98d3e-29b6-436a-a9e2-ae5da8f451a7",
    "visible": true
}