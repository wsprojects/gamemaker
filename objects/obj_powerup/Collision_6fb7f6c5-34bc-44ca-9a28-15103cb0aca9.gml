/// @author Wouter van der Heijden 30-3-2018
/// @description [Collision event obj_powerup]

with(other){
	powerup = true;
}
instance_destroy();