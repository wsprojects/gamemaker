{
    "id": "e1cfaf48-73c3-4721-8bf6-d332c5e65316",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_powerup",
    "eventList": [
        {
            "id": "6fb7f6c5-34bc-44ca-9a28-15103cb0aca9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6d5159ca-b493-4663-80f8-e36be2bcf31a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e1cfaf48-73c3-4721-8bf6-d332c5e65316"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7260cd62-2905-48b2-89de-ac838437dc3b",
    "visible": true
}