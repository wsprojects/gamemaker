{
    "id": "7f19edcb-9764-422a-a062-eb458303e72f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_purplespawn",
    "eventList": [
        {
            "id": "cff394e3-4641-43a3-8f2d-422064773225",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7f19edcb-9764-422a-a062-eb458303e72f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f0cc56ac-8f4f-424f-833d-2e2b56fc1bdc",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "28f3c13d-c2f3-459c-9f30-7a8f761597d0",
    "visible": true
}