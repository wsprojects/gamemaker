{
    "id": "d881630b-8380-4621-bfd0-14a571b3de52",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_yellowspawn",
    "eventList": [
        {
            "id": "89dcfb90-36e8-4e57-93b5-6437c1cb2d07",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d881630b-8380-4621-bfd0-14a571b3de52"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f0cc56ac-8f4f-424f-833d-2e2b56fc1bdc",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d948c471-a79d-4ec1-be12-be877a58d6de",
    "visible": true
}