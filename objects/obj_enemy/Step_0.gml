/// @author Wouter van der Heijden 30-3-2018
/// @description [Step event obj_enemy]

if (instance_exists(obj_player)) {
	move_towards_point(obj_player.x, obj_player.y, spd);	
}

image_angle = direction;

if (hp <= 0) {
	global.currentscore += value;
	audio_sound_pitch(snd_death, random_range(0.7,1.3));
	audio_play_sound(snd_death, 0, false);
	instance_create_layer(x, y, "ClutterLayer", deathObject);
	instance_destroy();
}

image_xscale = Wave(waveE, waveB, duration, 0);
image_yscale = image_xscale;