{
    "id": "dcf498de-075d-4ee7-bc6a-66c0dba2c1d1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy",
    "eventList": [
        {
            "id": "935e127d-fbe6-4551-a129-4ada6f553deb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dcf498de-075d-4ee7-bc6a-66c0dba2c1d1"
        },
        {
            "id": "8e0bf693-13be-4b1e-aee4-38316c02c0c8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dcf498de-075d-4ee7-bc6a-66c0dba2c1d1"
        },
        {
            "id": "4c1285ad-5535-4a1e-9590-14f220a5a440",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6d5159ca-b493-4663-80f8-e36be2bcf31a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "dcf498de-075d-4ee7-bc6a-66c0dba2c1d1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "aad589e9-fefc-45cd-8c07-305cf0b87498",
    "visible": true
}