/// @author Wouter van der Heijden 30-3-2018
/// @description [Draw event obj_bullet]

var cx = camera_get_view_x(view_camera[0]);
var cy = camera_get_view_y(view_camera[0]);
var cw = camera_get_view_width(view_camera[0]);

draw_set_font(fnt_score);
draw_set_colour(c_white);
draw_text(cx+cw/2-45,cy+45,"Score: "+string(global.currentscore));