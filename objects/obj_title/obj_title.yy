{
    "id": "4eba549f-2788-4898-ad5c-e882bc6979a6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_title",
    "eventList": [
        {
            "id": "d714e5e5-74d7-43f7-9ca6-40e278a8ba25",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4eba549f-2788-4898-ad5c-e882bc6979a6"
        },
        {
            "id": "62e2692c-b44e-42b5-9b5f-185b97a005c8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4eba549f-2788-4898-ad5c-e882bc6979a6"
        },
        {
            "id": "7cf1a4af-7cf5-4b76-a495-987d58064e53",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 10,
            "m_owner": "4eba549f-2788-4898-ad5c-e882bc6979a6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5a023c3f-1aad-449b-aac5-3dfbd5e195a5",
    "visible": true
}