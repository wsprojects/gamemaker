{
    "id": "fda0c886-931c-4392-93f0-a706c2ff24e9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_exitbutton",
    "eventList": [
        {
            "id": "aba4253e-13ea-4861-83cd-a9c173ce9045",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "fda0c886-931c-4392-93f0-a706c2ff24e9"
        },
        {
            "id": "ea116990-e3bf-4245-80fe-e838f0164264",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "fda0c886-931c-4392-93f0-a706c2ff24e9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "bb8784aa-8859-424f-b902-13a1c48086d1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0003f5f2-4bf6-4a9c-a54a-4b022dd9642a",
    "visible": true
}