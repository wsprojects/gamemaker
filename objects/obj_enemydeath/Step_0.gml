/// @author Wouter van der Heijden 31-3-2018
/// @description [Step event obj_enemydeath]

image_alpha = max(image_alpha-0.0005,0);

if (image_alpha == 0 ){
	instance_destroy();
}

if (new) {
	switch(irandom_range(0,2)) {
		default: break;
		case 0: sprite_index = spr1; break;
		case 1: sprite_index = spr2; break;
		case 2: sprite_index = spr3; break;
	}
	new = false;
}