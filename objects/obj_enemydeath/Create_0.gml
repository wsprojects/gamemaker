/// @author Wouter van der Heijden 31-3-2018
/// @description [Create event obj_enemydeath]

image_alpha = 1
new = true;

spr1 = spr_enemydeath1;
spr2 = spr_enemydeath2;
spr3 = spr_enemydeath3;