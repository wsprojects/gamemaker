{
    "id": "bbaa4e7d-c82d-4fc9-acf4-4b621b5acea4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemypurple",
    "eventList": [
        {
            "id": "8b18ca63-f5b2-494a-a981-594a5e28e94d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bbaa4e7d-c82d-4fc9-acf4-4b621b5acea4"
        },
        {
            "id": "7f66c51e-d9fa-4287-8799-5e42b4cd3a74",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bbaa4e7d-c82d-4fc9-acf4-4b621b5acea4"
        },
        {
            "id": "025ef6fb-a967-49b6-bc4a-149f64c0868d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6d5159ca-b493-4663-80f8-e36be2bcf31a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "bbaa4e7d-c82d-4fc9-acf4-4b621b5acea4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "28f3c13d-c2f3-459c-9f30-7a8f761597d0",
    "visible": true
}