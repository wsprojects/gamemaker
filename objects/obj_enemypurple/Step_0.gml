/// @author Wouter van der Heijden 30-3-2018
/// @description [Step event obj_enemy]

if (instance_exists(obj_player)) {
	move_towards_point(obj_player.x, obj_player.y, spd);	
}

image_angle = direction;

if (hp <= 0) {
	with(obj_score) {
		global.currentscore += 5;
	}
	audio_sound_pitch(snd_death, random_range(0.7,1.3));
	audio_play_sound(snd_death, 0, false);
	instance_create_layer(x, y, "ClutterLayer", deathObject);
	children = irandom_range(childcount[0],childcount[1]);
	while (children > 0){
		instance_create_layer(x+irandom_range(-120,120), y+irandom_range(-120,120), "EnemyLayer", child);
		children--;
	}
	instance_destroy();
}

image_xscale = Wave(waveE, waveB, duration, 0);
image_yscale = image_xscale;