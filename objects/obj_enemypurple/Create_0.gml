/// @author Wouter van der Heijden 30-3-2018
/// @description [Create event obj_enemy]

hp = 15;
spd = 1.2;
value = 25;
deathObject = obj_purpledeath;


waveE = 1.65;
waveB = 1.9;
duration = 2.5;

child = obj_yellowspawn;
childcount = [4,10];
