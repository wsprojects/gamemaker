/// @author Wouter van der Heijden 30-3-2018
/// @description [Step event obj_player]


//Movement
d = [0.0,0.0];
if (keyboard_check(ord("D"))) d[0] += 1;
if (keyboard_check(ord("A"))) d[0] -= 1;
if (keyboard_check(ord("W"))) d[1] -= 1;
if (keyboard_check(ord("S"))) d[1] += 1;
l = sqrt(d[0] * d[0] + d[1] * d[1])
if(l!=0){
x += d[0] * mspeed / l;
y += d[1] * mspeed / l;
}

//Image angle
image_angle = point_direction(x, y, mouse_x, mouse_y) + 90;


//Bullet
if (mouse_check_button(mb_left)) && (reset < 1)
{
	instance_create_layer(x, y, "BulletsLayer", obj_bullet);
	reset = cooldown;
}
reset -= 1

x = clamp(x, 50, room_width-50);
y = clamp(y, 50, room_height-50);

if(powerup){
	switch(irandom_range(0,1)) {
		case 0: mspeed++; break;
		case 1: if(cooldown >= 1) {cooldown--;} else {mspeed++;} break;
		default: break;
	}
	powerup = false;
}
