{
    "id": "6d5159ca-b493-4663-80f8-e36be2bcf31a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "dc074a2c-0013-49b6-9750-7217087a4e53",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6d5159ca-b493-4663-80f8-e36be2bcf31a"
        },
        {
            "id": "6d725776-615a-4e4d-a88e-50a17f5c6f84",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6d5159ca-b493-4663-80f8-e36be2bcf31a"
        },
        {
            "id": "e839843a-3af5-4f3b-a69d-a2f66c5851bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 67,
            "eventtype": 9,
            "m_owner": "6d5159ca-b493-4663-80f8-e36be2bcf31a"
        },
        {
            "id": "3cfdbded-6414-45ee-8831-c9c31c308044",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6d5159ca-b493-4663-80f8-e36be2bcf31a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "26759a74-c2f8-4fd8-a3fa-1e2aa953064d",
    "visible": true
}