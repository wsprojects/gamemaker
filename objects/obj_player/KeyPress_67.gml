/// @author Wouter van der Heijden 30-3-2018
/// @description [Key Press - C obj_player]

switch (skindex) {
	case 0: sprite_index = spr_player; skindex++; break;
	case 1: sprite_index = spr_player_dev; skindex--; break;
	default: break;
}
