{
    "id": "9f9d0daa-4295-4378-8ec5-86d55bb5fd82",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemyyellow",
    "eventList": [
        {
            "id": "5399e4a2-33cb-4ef3-84d1-10c950571def",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9f9d0daa-4295-4378-8ec5-86d55bb5fd82"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "dcf498de-075d-4ee7-bc6a-66c0dba2c1d1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d948c471-a79d-4ec1-be12-be877a58d6de",
    "visible": true
}