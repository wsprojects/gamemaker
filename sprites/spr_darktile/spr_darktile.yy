{
    "id": "8dc5a8cd-dee3-4bdb-aef6-2d9a8d16d99e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_darktile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c874d303-5654-4afc-aa9d-a676269b83e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8dc5a8cd-dee3-4bdb-aef6-2d9a8d16d99e",
            "compositeImage": {
                "id": "e1a0748e-0201-4a92-843d-ee44358a558d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c874d303-5654-4afc-aa9d-a676269b83e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed13dae3-8735-43c5-9d09-9680a2518860",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c874d303-5654-4afc-aa9d-a676269b83e5",
                    "LayerId": "3ed9c4d6-d270-4bb7-876f-29a72e2cff92"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "3ed9c4d6-d270-4bb7-876f-29a72e2cff92",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8dc5a8cd-dee3-4bdb-aef6-2d9a8d16d99e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}