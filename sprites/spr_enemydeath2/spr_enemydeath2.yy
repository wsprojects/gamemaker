{
    "id": "be649895-9406-4de8-8f70-59abbcf8e972",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemydeath2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 101,
    "bbox_left": 16,
    "bbox_right": 103,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f63145ed-abd6-4889-8d40-7d01d3bc3abf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be649895-9406-4de8-8f70-59abbcf8e972",
            "compositeImage": {
                "id": "47519e3a-bd63-478d-9e0d-3f0a5523f576",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f63145ed-abd6-4889-8d40-7d01d3bc3abf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bb6c2c0-c297-4c23-9282-4a1422a56559",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f63145ed-abd6-4889-8d40-7d01d3bc3abf",
                    "LayerId": "94f9bfb2-e36b-42a0-9431-bfe4ef107824"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 114,
    "layers": [
        {
            "id": "94f9bfb2-e36b-42a0-9431-bfe4ef107824",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "be649895-9406-4de8-8f70-59abbcf8e972",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 115,
    "xorig": 57,
    "yorig": 57
}