{
    "id": "5a34c414-a0b9-42de-8c9f-6abe7863360a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 42,
    "bbox_right": 76,
    "bbox_top": 39,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0b33d173-6d98-4eca-8c95-44db6b3a3bfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a34c414-a0b9-42de-8c9f-6abe7863360a",
            "compositeImage": {
                "id": "e38d58ff-61a2-4026-b7ff-85fd8cc0537e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b33d173-6d98-4eca-8c95-44db6b3a3bfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40b39c25-6799-4eba-9d89-372eb914afe6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b33d173-6d98-4eca-8c95-44db6b3a3bfd",
                    "LayerId": "19b26fd5-8319-4087-9f97-a93c7445f840"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 114,
    "layers": [
        {
            "id": "19b26fd5-8319-4087-9f97-a93c7445f840",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5a34c414-a0b9-42de-8c9f-6abe7863360a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 115,
    "xorig": 0,
    "yorig": 0
}