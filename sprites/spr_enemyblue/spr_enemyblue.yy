{
    "id": "e1a98d3e-29b6-436a-a9e2-ae5da8f451a7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemyblue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 85,
    "bbox_left": 29,
    "bbox_right": 83,
    "bbox_top": 36,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "63065799-e872-45a2-92ee-4005955d91da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1a98d3e-29b6-436a-a9e2-ae5da8f451a7",
            "compositeImage": {
                "id": "be2ffd8c-3801-45c2-838d-b1a40e0a021e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63065799-e872-45a2-92ee-4005955d91da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d642df1-0be5-477f-9daa-9c8face6b6cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63065799-e872-45a2-92ee-4005955d91da",
                    "LayerId": "6613ad6d-1b10-483c-b472-0865987a994e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 114,
    "layers": [
        {
            "id": "6613ad6d-1b10-483c-b472-0865987a994e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e1a98d3e-29b6-436a-a9e2-ae5da8f451a7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 115,
    "xorig": 57,
    "yorig": 57
}