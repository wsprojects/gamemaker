{
    "id": "e10d2bb0-5374-4d23-be14-e52f2158003b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_purpledeath",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 101,
    "bbox_left": 16,
    "bbox_right": 103,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dff11bbf-e63f-4049-a90e-5803c01ca917",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e10d2bb0-5374-4d23-be14-e52f2158003b",
            "compositeImage": {
                "id": "60a2137f-a0e9-459d-83f2-6300e485a054",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dff11bbf-e63f-4049-a90e-5803c01ca917",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55ae1986-6b48-482b-818e-edba8aa46882",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dff11bbf-e63f-4049-a90e-5803c01ca917",
                    "LayerId": "36f97a30-b7ad-4956-aabc-bd5aed5fd2a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 114,
    "layers": [
        {
            "id": "36f97a30-b7ad-4956-aabc-bd5aed5fd2a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e10d2bb0-5374-4d23-be14-e52f2158003b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 115,
    "xorig": 57,
    "yorig": 57
}