{
    "id": "05f8322b-29e4-40d5-a05a-3b2f04bb7092",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_dev",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 76,
    "bbox_left": 36,
    "bbox_right": 77,
    "bbox_top": 36,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3743f39e-0ea3-4270-b133-7b51f1c055c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05f8322b-29e4-40d5-a05a-3b2f04bb7092",
            "compositeImage": {
                "id": "b34ab44b-f71c-482f-90ac-bef35c22e58a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3743f39e-0ea3-4270-b133-7b51f1c055c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f55f92e8-61e6-4241-946d-21bc5fc8758b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3743f39e-0ea3-4270-b133-7b51f1c055c3",
                    "LayerId": "0265e553-1398-4aaf-87a2-18eb0ed86489"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 115,
    "layers": [
        {
            "id": "0265e553-1398-4aaf-87a2-18eb0ed86489",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "05f8322b-29e4-40d5-a05a-3b2f04bb7092",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 114,
    "xorig": 57,
    "yorig": 57
}