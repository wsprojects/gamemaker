{
    "id": "1342e9b1-a912-4bd8-a294-372fe0eedd97",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bluedeath",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 104,
    "bbox_left": 13,
    "bbox_right": 100,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "27b5d757-ae6d-4a2a-989b-b149e8b636b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1342e9b1-a912-4bd8-a294-372fe0eedd97",
            "compositeImage": {
                "id": "028ee162-088d-441a-bb9a-0de0283cc521",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27b5d757-ae6d-4a2a-989b-b149e8b636b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1013e0d-dc1b-4945-bb86-0df729053cf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27b5d757-ae6d-4a2a-989b-b149e8b636b7",
                    "LayerId": "1850f943-2d98-4da3-9808-71ab1dfdf73a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 114,
    "layers": [
        {
            "id": "1850f943-2d98-4da3-9808-71ab1dfdf73a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1342e9b1-a912-4bd8-a294-372fe0eedd97",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 115,
    "xorig": 57,
    "yorig": 57
}