{
    "id": "d948c471-a79d-4ec1-be12-be877a58d6de",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemyyellow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 83,
    "bbox_left": 34,
    "bbox_right": 78,
    "bbox_top": 36,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cdbe4700-0e8e-4398-9577-4c005cb45862",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d948c471-a79d-4ec1-be12-be877a58d6de",
            "compositeImage": {
                "id": "384d7fcb-e9d9-4501-be39-824baefcfc2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdbe4700-0e8e-4398-9577-4c005cb45862",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b765bfa1-c57a-4cdf-9bc9-71f1e4f90591",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdbe4700-0e8e-4398-9577-4c005cb45862",
                    "LayerId": "8c4bd029-8116-4e59-9cb3-10b2490b1be5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 114,
    "layers": [
        {
            "id": "8c4bd029-8116-4e59-9cb3-10b2490b1be5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d948c471-a79d-4ec1-be12-be877a58d6de",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 115,
    "xorig": 57,
    "yorig": 57
}