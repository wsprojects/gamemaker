{
    "id": "26759a74-c2f8-4fd8-a3fa-1e2aa953064d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 82,
    "bbox_left": 33,
    "bbox_right": 80,
    "bbox_top": 34,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 87,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "09c5a482-fe7a-4ec8-80a4-cfc22622112e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26759a74-c2f8-4fd8-a3fa-1e2aa953064d",
            "compositeImage": {
                "id": "40e86449-efd9-449a-9f88-b078794c1f45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09c5a482-fe7a-4ec8-80a4-cfc22622112e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a849303e-2195-4cfb-85ee-51c5cea101ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09c5a482-fe7a-4ec8-80a4-cfc22622112e",
                    "LayerId": "17ca163f-7fd8-4d1f-9870-dc58e9c58fe3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 115,
    "layers": [
        {
            "id": "17ca163f-7fd8-4d1f-9870-dc58e9c58fe3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "26759a74-c2f8-4fd8-a3fa-1e2aa953064d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 114,
    "xorig": 57,
    "yorig": 57
}