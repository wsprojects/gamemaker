{
    "id": "0003f5f2-4bf6-4a9c-a54a-4b022dd9642a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 0,
    "bbox_right": 303,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c4ad2459-c160-42b0-ba02-c541fe1d9fe7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0003f5f2-4bf6-4a9c-a54a-4b022dd9642a",
            "compositeImage": {
                "id": "b2ef7774-c6d8-4756-a89c-d4608c6e626b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4ad2459-c160-42b0-ba02-c541fe1d9fe7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a1fcf05-1004-4930-af6b-f325b42e3324",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4ad2459-c160-42b0-ba02-c541fe1d9fe7",
                    "LayerId": "2e26708b-4222-40bc-9297-a8f9a56e500d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "2e26708b-4222-40bc-9297-a8f9a56e500d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0003f5f2-4bf6-4a9c-a54a-4b022dd9642a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 304,
    "xorig": 152,
    "yorig": 41
}