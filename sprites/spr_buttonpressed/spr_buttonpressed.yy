{
    "id": "2ac4436b-62b7-4bb0-b5fe-d7811c526b62",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_buttonpressed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 72,
    "bbox_left": 0,
    "bbox_right": 292,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1d865ed9-950b-48eb-a98f-f02e124f05f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ac4436b-62b7-4bb0-b5fe-d7811c526b62",
            "compositeImage": {
                "id": "941aa16c-7ccf-4d21-b816-3373da5e2751",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d865ed9-950b-48eb-a98f-f02e124f05f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58c385fe-50eb-466f-b9ed-a5faf80b7ca2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d865ed9-950b-48eb-a98f-f02e124f05f3",
                    "LayerId": "f2050329-596f-4132-9a6c-397daec431f6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 73,
    "layers": [
        {
            "id": "f2050329-596f-4132-9a6c-397daec431f6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2ac4436b-62b7-4bb0-b5fe-d7811c526b62",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 293,
    "xorig": 146,
    "yorig": 36
}