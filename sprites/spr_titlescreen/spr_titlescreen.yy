{
    "id": "5a023c3f-1aad-449b-aac5-3dfbd5e195a5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_titlescreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 256,
    "bbox_left": 0,
    "bbox_right": 859,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fdf999e2-d1ba-4a32-ab90-a87ab613b43c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a023c3f-1aad-449b-aac5-3dfbd5e195a5",
            "compositeImage": {
                "id": "8d3beb77-fc1e-4147-96a6-375eb0537730",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdf999e2-d1ba-4a32-ab90-a87ab613b43c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "017b0116-8fa1-4bb7-9f2c-af58603768e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdf999e2-d1ba-4a32-ab90-a87ab613b43c",
                    "LayerId": "79d0cf46-f534-4de8-8d16-8f121d2ba308"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 283,
    "layers": [
        {
            "id": "79d0cf46-f534-4de8-8d16-8f121d2ba308",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5a023c3f-1aad-449b-aac5-3dfbd5e195a5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 860,
    "xorig": 430,
    "yorig": 141
}