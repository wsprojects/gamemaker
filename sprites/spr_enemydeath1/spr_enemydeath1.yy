{
    "id": "7058f261-b88a-4585-a01c-9578c60af756",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemydeath1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 101,
    "bbox_left": 16,
    "bbox_right": 97,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "75ffb5f2-bfd4-4f4d-9fd2-6889762705b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7058f261-b88a-4585-a01c-9578c60af756",
            "compositeImage": {
                "id": "98c9df89-8d9a-46b4-bfb9-ca2788b02856",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75ffb5f2-bfd4-4f4d-9fd2-6889762705b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "317814bb-8069-49fd-b864-69a1aed43bfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75ffb5f2-bfd4-4f4d-9fd2-6889762705b2",
                    "LayerId": "62e4aaa9-cbab-4c57-a00f-006c81572015"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 114,
    "layers": [
        {
            "id": "62e4aaa9-cbab-4c57-a00f-006c81572015",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7058f261-b88a-4585-a01c-9578c60af756",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 115,
    "xorig": 57,
    "yorig": 57
}