{
    "id": "3f4189e8-ddee-45fe-9d96-c6cd0e5e7d11",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_yellowdeath",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 101,
    "bbox_left": 16,
    "bbox_right": 97,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a9c200a3-2b66-4180-a7b4-b0ec65544b89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f4189e8-ddee-45fe-9d96-c6cd0e5e7d11",
            "compositeImage": {
                "id": "2df29759-5389-45a0-b0bb-1163878d54f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9c200a3-2b66-4180-a7b4-b0ec65544b89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30ded320-f502-443e-9804-7d4a3ca1b7c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9c200a3-2b66-4180-a7b4-b0ec65544b89",
                    "LayerId": "9a9e9d20-0d61-4f18-9d06-36cfa6ca91d6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 114,
    "layers": [
        {
            "id": "9a9e9d20-0d61-4f18-9d06-36cfa6ca91d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f4189e8-ddee-45fe-9d96-c6cd0e5e7d11",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 115,
    "xorig": 57,
    "yorig": 57
}