{
    "id": "8895c669-d572-485b-ba0e-27966a53ae2d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 383,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1d1ef05f-9557-457b-bee1-286333f3e8e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8895c669-d572-485b-ba0e-27966a53ae2d",
            "compositeImage": {
                "id": "bc4862df-552b-42b2-b6d8-a26988e9b2e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d1ef05f-9557-457b-bee1-286333f3e8e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99aabb58-12f5-4966-9f04-fc3e85c479d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d1ef05f-9557-457b-bee1-286333f3e8e9",
                    "LayerId": "b90135cb-b6f6-4b3d-846d-5a323eed2d02"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "b90135cb-b6f6-4b3d-846d-5a323eed2d02",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8895c669-d572-485b-ba0e-27966a53ae2d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 384,
    "xorig": 0,
    "yorig": 0
}