{
    "id": "7260cd62-2905-48b2-89de-ac838437dc3b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_powerup",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 0,
    "bbox_right": 36,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "65ca4ec7-88ab-4128-8aae-1d657591dc10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7260cd62-2905-48b2-89de-ac838437dc3b",
            "compositeImage": {
                "id": "432ee02a-056f-4069-afc5-e4558b231777",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65ca4ec7-88ab-4128-8aae-1d657591dc10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7e55192-3b2b-41ae-bed1-f234f00f8bb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65ca4ec7-88ab-4128-8aae-1d657591dc10",
                    "LayerId": "5010bb55-684f-4758-afc3-94c52aae368d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 42,
    "layers": [
        {
            "id": "5010bb55-684f-4758-afc3-94c52aae368d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7260cd62-2905-48b2-89de-ac838437dc3b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 37,
    "xorig": 18,
    "yorig": 21
}