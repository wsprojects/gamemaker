{
    "id": "8de89e85-d36c-44d9-9015-0667a326b5bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemydeath3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 104,
    "bbox_left": 13,
    "bbox_right": 100,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "05a9a525-9039-49ee-9e29-6d01022f92ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8de89e85-d36c-44d9-9015-0667a326b5bb",
            "compositeImage": {
                "id": "36637116-2470-4139-8ea2-ef027596a5ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05a9a525-9039-49ee-9e29-6d01022f92ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8da031bf-e2ca-4273-9c64-00a61e913913",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05a9a525-9039-49ee-9e29-6d01022f92ae",
                    "LayerId": "c5c782d2-5543-44b5-930a-2180b7641894"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 114,
    "layers": [
        {
            "id": "c5c782d2-5543-44b5-930a-2180b7641894",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8de89e85-d36c-44d9-9015-0667a326b5bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 115,
    "xorig": 57,
    "yorig": 57
}