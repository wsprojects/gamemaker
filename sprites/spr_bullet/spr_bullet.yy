{
    "id": "a8c0d942-b6e0-431f-97ee-090d42838226",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 70,
    "bbox_left": 98,
    "bbox_right": 109,
    "bbox_top": 57,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 72,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c2d563c-53c6-4f7a-9a10-f0574edfad30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8c0d942-b6e0-431f-97ee-090d42838226",
            "compositeImage": {
                "id": "9524bdee-b46b-48d2-92d1-fd311c094c4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c2d563c-53c6-4f7a-9a10-f0574edfad30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2755224f-0341-488a-8c2a-fb4a0c6fbd24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c2d563c-53c6-4f7a-9a10-f0574edfad30",
                    "LayerId": "f192d7c4-1b13-440b-a235-a553034c389d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "f192d7c4-1b13-440b-a235-a553034c389d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a8c0d942-b6e0-431f-97ee-090d42838226",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 102,
    "yorig": 63
}