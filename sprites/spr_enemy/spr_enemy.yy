{
    "id": "aad589e9-fefc-45cd-8c07-305cf0b87498",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 85,
    "bbox_left": 30,
    "bbox_right": 81,
    "bbox_top": 33,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 219,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "35c69e24-160d-4b69-be3a-0ef31756a403",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aad589e9-fefc-45cd-8c07-305cf0b87498",
            "compositeImage": {
                "id": "800b1299-22e5-4e86-b41f-5586f5b663ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35c69e24-160d-4b69-be3a-0ef31756a403",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b95afce-e06b-41bf-a79f-3f543faef893",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35c69e24-160d-4b69-be3a-0ef31756a403",
                    "LayerId": "713c30bd-edba-4255-988b-687f20eb6006"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 114,
    "layers": [
        {
            "id": "713c30bd-edba-4255-988b-687f20eb6006",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aad589e9-fefc-45cd-8c07-305cf0b87498",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 115,
    "xorig": 57,
    "yorig": 57
}