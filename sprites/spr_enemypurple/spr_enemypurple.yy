{
    "id": "28f3c13d-c2f3-459c-9f30-7a8f761597d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemypurple",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 86,
    "bbox_left": 28,
    "bbox_right": 82,
    "bbox_top": 32,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c07efde4-38cd-4953-9adb-47398724b35d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28f3c13d-c2f3-459c-9f30-7a8f761597d0",
            "compositeImage": {
                "id": "f3423a7d-286e-4f93-93f6-06a97a3d24b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c07efde4-38cd-4953-9adb-47398724b35d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87eab144-fbe3-4d17-b2d6-d0107851b46e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c07efde4-38cd-4953-9adb-47398724b35d",
                    "LayerId": "b160da94-a555-425d-8b5b-e08fa923a06b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 114,
    "layers": [
        {
            "id": "b160da94-a555-425d-8b5b-e08fa923a06b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "28f3c13d-c2f3-459c-9f30-7a8f761597d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 115,
    "xorig": 57,
    "yorig": 57
}