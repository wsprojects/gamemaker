{
    "id": "418db765-d53a-455c-a05e-0cfc113e9dfd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skull",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 205,
    "bbox_left": 0,
    "bbox_right": 218,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8b83ebd9-7c63-424d-8c20-6f207f34c1a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "418db765-d53a-455c-a05e-0cfc113e9dfd",
            "compositeImage": {
                "id": "e0c8cdb1-cfd4-45ab-b3fb-3c1657858364",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b83ebd9-7c63-424d-8c20-6f207f34c1a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa62d37f-5a5c-4171-b3be-ddaef73a931d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b83ebd9-7c63-424d-8c20-6f207f34c1a2",
                    "LayerId": "979975c2-2221-481d-905e-2f21438dbad9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 206,
    "layers": [
        {
            "id": "979975c2-2221-481d-905e-2f21438dbad9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "418db765-d53a-455c-a05e-0cfc113e9dfd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 219,
    "xorig": 109,
    "yorig": 103
}